package stepDefinitions;

import cucumber.api.PendingException;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import data.RealizarPedidoData;
import org.springframework.beans.factory.annotation.Autowired;
import pageObjects.RealizarPedidoPage;

public class FormaDePagamentoSteps {

    @Autowired
    private RealizarPedidoPage page;
    private RealizarPedidoData dados;


    @Dado("^que o usuário realizou seu pedido$")
    public void queousuáriorealizouseupedido() throws InterruptedException {
        page.fecharPedido();


    }

    @Quando("^o usuário preenche todos os campos obrigatórios$")
    public void o_usuário_preenche_todos_os_campos_obrigatórios() throws Throwable {
        page.setDados();

    }

    @Quando("^seleciona o tipo de pagamento \"([^\"]*)\"$")
    public void seleciona_o_tipo_de_pagamento(String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Quando("^o pedido do usuário foi concluido$")
    public void o_pedido_do_usuário_foi_concluido() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Então("^o sistema deve informar a mensagem \"([^\"]*)\"Pedido Concluído - Seu pedido foi recebido pelo restaurante\\. Prepare sua mesa que a comida está chegando!\"([^\"]*)\"$")
    public void o_sistema_deve_informar_a_mensagem_Pedido_Concluído_Seu_pedido_foi_recebido_pelo_restaurante_Prepare_sua_mesa_que_a_comida_está_chegando(String arg1, String arg2) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
}
