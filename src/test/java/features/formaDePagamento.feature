#language: pt
Funcionalidade: Realizar Pedido

    Narrativa:
    Como um cliente que adora comer gostaria de realizar o pedido do meu prato favorito
    Para que possa ser entregue no endereço desejado e seja pago na forma de pagamento de minha escolha.

    @WEB
    @wip
    Esquema do Cenário: Forma de pagamento
        Dado que o usuário realizou seu pedido
        Quando o usuário preenche todos os campos obrigatórios
        E seleciona o tipo de pagamento "<Tipo>"
        E o pedido do usuário foi concluido
        Então o sistema deve informar a mensagem "<Mensagem>"

        Exemplos:
            | Tipo             | Mensagem                                                                                                    |
            | Dinheiro         | "Pedido Concluído - Seu pedido foi recebido pelo restaurante. Prepare sua mesa que a comida está chegando!" |
 #           | Cartão de Débito | "Pedido Concluído - Seu pedido foi recebido pelo restaurante. Prepare sua mesa que a comida está chegando!" |
 #           | Cartão Refeição  | "Pedido Concluído - Seu pedido foi recebido pelo restaurante. Prepare sua mesa que a comida está chegando!" |