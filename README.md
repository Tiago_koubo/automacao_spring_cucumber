Requisitos para o projeto de automação:

Rodar de forma parametrizável em diferentes browsers (Chrome, Firefox, IE, headless)
Tirar screenshot em caso de falha do teste
Utilizar Cucumber
Executar em alguma ferramenta de Integração Contínua
Executar utilizando https://github.com/zalando/zalenium

Aplicação para fazer os testes: https://github.com/cod3rcursos/meat-app-final

Funcionalidade: Realizar Pedido

	Como um cliente que adora comer
	Gostaria de realizar o pedido do meu prato favorito
	Para que possa ser entregue no endereço desejado 
	E seja pago na forma de pagamento de minha escolha


	Critérios de aceite:

	- Campos obrigatórios:
		- Nome
		- E-mail
		- Confirmação do e-mail
		- Endereço
		- Número

	- Quantidade mínima de caracteres
		 Nome: 5 caracteres
		Endereço: caracteres

	- Itens
		- Não deve ser permitido prosseguir com o pedido caso tenha quantidade zero em qualquer item desejado

	- Valores
		- Todos valores devem conter "R$"
		- Campo total recebe a soma dos itens com o frete (Total = Itens + Frete)
		- Campo Subtotal recebe o valor unitário do produto multiplicado pela quantidade (Subtotal = Valor Unitário * Quantidade)

	- Forma de pagamento
		- Dinheiro, Cartão de Débito, Cartão Refeição

	- Mensagens
		- Conclusão do Pedido
			- "Pedido Concluído - Seu pedido foi recebido pelo restaurante. Prepare sua mesa que a comida está chegando!"


Funcionalidade: Pesquisar Restaurante

	Como um cliente que adora comer
	Gostaria de pesquisar um restaurante de minha preferência
	Para que possa visualizar as informações sobre o estabelecimento
	E o cardápio online



	Critérios de aceite:

	- Campos obrigatórios:
		- Campo para busca do restaurante

	- Valores
		- Todos valores devem conter "R$"

	- Dados do estabelecimento
		- Categoria (para saber qual a especialidade)gym
		- Quem somos (para saber se tem know how no mercado)
		- Horários (para saber qual horário de funcionamento)

	- Cardápio
		- Itens devem ser exibidos em forma de lista
		- Imagem alinhada a esquerda
		- Nome do prato em caixa alta
		- Breve descrição do prato abaixo do nome
		- Botão adicionar deve estar sendo exibido